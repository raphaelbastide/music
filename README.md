music
=====
The music I listen to.

## Pop rock / Indie

* The zombies
* The cat empire
* Chicago Transit
* The clash
* The white stripes
* Jeff Buckley
* Elliott smith
* Sonic Youth
* The Velvet Underground
* Lou Reed
* Queen
* Guns'n Roses
* Radiohead
* Amy winehouse
* Bjork
* G. Love & Special Sauce
* The Jesus and Mary Chain
* Led Zeppelin
* Richard Swift
* Jefferson airplane
* The Doors
* Pixies
* David Bowie
* Jimmy Page
* Jimy Hendrix
* Oh!Tiger Mountain
* Supertramp
* Jamie Cullom
* Franck Zappa
* Can
* La Monte Young
* The Smiths
* Morrissey
* Pavement
* The dodos
* Colour Haze
* Electric Light Orchestra
* PJ Harvey
* Why?
* Canned heat
* Grateful dead
* Julie Driscoll
* Os Mutantes
* Strokes
* Charlatans
* Salad
* Blur
* Oasis
* Electrelaine

## Experimental / alt rock

* Liars
* Snakefingers
* Captain Beefheart
* Renaldo & The Loaf
* The residents
* Fred Frith
* master musicians of bukkake
* Gablé
* Hanne Hukkelberg
* Antony and the Johnsons
* Connan Mockasin
* Panda Bear
* Majeure

## Punk

* Black Flag
* The Sex Pistols
* Patti Smith
* Discharge
* New York Dolls
* NOFX
* Ramones
* The clash
* Pussy Riot
* Crash Course In Science

## Folk / Country

* Johnny Cash
* Leonard Cohen
* Bob Dylan
* Neil Young
* Woody Guthrie
* Pete Seeger
* Odetta
* Stephen Steinbrink
* Herman Düne
* Sharon Van Etten
* Eva Cassidy
* Peter Von Poehl
* Mountain man
* Tom Whaits
* Ghost Days
* Bon Iver
* Tiny Ruins

## Tradition

* La mal coiffée
* the pogues
* Sinéad O'Connor
* Shirley Collins

## Acousmatique / Concrète

* Pauline Oliveros
* John Cage
* Yannick Dauby
* Philippe Mion
* Bernard Parmegiani
* Christian Zanesi
* Michel Redolfi

## Sérielle

* Arnold Schönberg
* Alban Berg
* Anton Webern
* François Leclère

## Minimal / Nerdy

* Brian eno
* Philip Glass
* Michael Pisaro
* John Adams
* Terry Riley
* Michael Nyman
* Sand (1974)
* Don Preston
* Lee Ranaldo
* Renato rinaldi
* William Winant
* Jeff Carey
* Nils Frahm
* Barry Truax
* Arvo Pärt
* Steve Reich
* Richard Skelton
* Jon Hassel
* David Lang
* Moondog
* Nico Muhly
* SND

## Mathrock

* Foals
* Don Caballero
* Vessels
* Below the Sea
* electric electric
* 65daysofstatic
* Lite
* Tera Melos
* Nomeansno
* Breadwinner

## “World”

* Bresil Casuarina
* O Karaíva
* Harry Belafonte
* Senor coconut
* Trio Chemirani / Omar sosa trio / Ballaké Sissoko
* Omar Sosa
* Carmen Souza
* http://www.awesometapes.com/
* Bangalafumenga
* Novos Baianos
* Tinarwen

### Benin

* Orchestre Poly-Rythmo de Cotonou

### Niger

* Fela Kuti

### Cameroun

* Etienne Mbappe
* Ballake Sissoko
* Henri Dikongue
* Le groupe Essiimo
* Charlotte M'Bango
* Francis Bebey

### Ethiopie

* Mulatu Astatke

### Gambie

* Justin Adams & Juldeh Camara

### Guinée

* Orchestre du Jardin de Guinée

### Madagascar

* Damily

### Congo

* Papa Kourand
* Staff Benda Bilili

### Centrafrique

* http://www.4-shared.eu/mediafire%20zip%20ocora

### Mali

* Rokia Traoré
* Salif Keita
* Oumou sangare
* Ali Farka Touré
* Côte d'Ivoire
* François Lougah
* Style ziglibithy

### Réeunion

* Alain Peters
* Danyèl Waro

### Cuba

* Chucho Valdés
* Compay Segundo
* Calle 54
* Michel Camilo
* Paquito D'Rivera
* Tito Puente
* Bebo Valdés

### Bresil

* Astrud Gilberto
* Joao Gilberto
* Carlos Lyra
* Marcos Valle
* Caetano Veloso
* Antonio Carlos Jobim
* Edu Lobo
* Tom Jobim
* Elis Regina
* Maria Bethânia
* Eliane Radigue

### Inde

* Pandit Pran Nath

## Jazz

* Aaron Parks
* Davis Sànchez (Traversia)
* James Farm
* Frères Moutin
* George Adams - Don Pullen quartet
* Art Ensemble Of Chicago
* Art Blakey
* Dave douglas
* Peter Brötzmann
* weather report 
* Sun Ra
* roberto fonseca
* Daniel Humair
* Eric Dolphy
* The Bad Plus
* Sam Crockatt Quartet

### Sax

* Steve Coleman
* Joshua Redman
* Ornette Coleman
* Von Freeman
* John Coltrane
* Matana Roberts
* Wayne Shorter
* Colin Stetson

### Piano

* Lonnie Smith
* Ryuichi Sakamoto
* Monty Alexander
* Paul Bley
* Abdullah Ibrahim, aka Dollar Brand
* Chick Corea
* Ahmad Jamal
* Thelonious Monk
* McCoy Tiner
* Brad Mehldau
* Jean-michel pilc
* Antoine Hervé
* Keith Jarrett
* Jacques Loussier
* Jacky Terrasson
* Cecil Percival Taylor
* François Couturier

### Contrebasse

* Ron Carter
* Charles Mingus
* Dave Holland
* Pau Chambers
* Christian Mc Bride
* Jaco pastorius
* Henry Texier

### Guitare

* Biréli Lagrène
* Ernest Rangling
* Sylvain Luc
* Eddie Lang
* Stochelo Rosenberg / Rosenberg Trio
* Django Reinhardt
* Jonhatan kriesberg
* Lague lund
* Mike moreno
* Kurt roseninkel
* Tomy emmanuel
* Stanley Jordan
* Didier Ashour

## Classique

* Heinrich Ignaz Franz Biber
* Céline Frisch
* Ludwig van Beethoven
* Frédéric Chopin
* Claude Debussy
* Bela Bartók
* Eric Satie
* Pyotr Ilyich Tchaikovsky
* Franz Schubert
* Johannes Brahms
* Robert Schumann
* Franz Liszt
* Glen Gould
* Igor Stravinsky
* Francis Poulenc
* Hector Berlioz
* David El-Malek

### Guitare

* Mauro Giuliani
* Sylvius Leopold Weiss (luth)
* Contemporain
* Arnold Schönberg
* Iannis Xenakis
* Carl Orff
* György Sándor Ligeti

## Blues

* James Cotton
* Skip James
* Ram Jam
* Keb' Mo'
* Muddy Waters
* Delta Blues
* Robert Johnson
* John Lee 'SonnyBoy' (I) Williamson
* Sonny Terry
* Sonny Boy Williamson (II)
* Alan Lomax: Negro Prison Blues & Songs

## Rhythm and blues / soul

* Nina Simone
* Areta Franklin
* Bill Withers

## Funk

* Lonnie Liston Smith
* Electric Cowbell Records
* Karl Hector & the Malcouns
* Jamiroquai
* James Brown
* Youngblood Brass Band
* Maceo Parker
* Jack McDuff
* Richard “Groove” Holmes
* Jimmy McGriff
* Vulfpeck

## Electro

* James Blake
* Kraftwerk
* Karl Bartos
* Conrad Schnitzler
* Kid Koala
* Amon Tobin
* Jon Hopkins
* Luke Vibert
* Aphex Twin
* Sandwell District
* Yves De Mey
* Benjamin MILLER
* Laurie Spiegel
* General Eclectic (hardcore)
* Brunil Ferrari
* Pierre Henry
* Takahisa Mitsumori
* Takahisa kosugi
* mileece
* borful tang
* Cindytalk
* Stumbleine
* Morton Subotnick
* Baconhead
* Skrillex
* Heddy Boubaker
* florent colautti
* Ben Frost
* Dinos Chapman
* Richard Devine
* Mergrim
* James Holden
* Para One
* Holly Herndon
* Jacques

### Footwork

* dj rashaad?

### Mathematica

* Martin Neukom http://www.domizil.ch/releases/d25/d25.html
* Pan Sonic (mika vainio)
* Mr Oizo
* Jam city
* Gilles Gobeil
* Cristian Zanési
* Oneohtrix Point Never
* Isan
* Dick Raaymakers aka Kid Baltan - studios Philips
* Darkside
* Ryoji Ikeda

### Electronica

* Nebulo
* Igorrr
* Franck Vigroux
* Alva Noto (Carsten Nicolai)
* Emptyset
* Julien Adrien Lacroix
* Antti Rannisto
* Kangding Ray
* Aoki Takamasa

### Soft / Ambiant / Cheap

* Ecoplan
* Bryter Layter
* Chiptune / 8/16bits
* Jahtari
* Souleye
* Jim Guthrie
* Max Tundra
* Gangpol & Mit
* Nero’s Day At Disneyland
* Internal Sun
* Tim Hecker
* Grouper
* Julianna Barwick
* William Basinski
* Jan Jelinek

### Dance / New, Cold, minimal wave

* kavinsky
* ortrotasce
* Suzy Andrews
* the simple minds
* radio WE ARE NOT TOYS
* M83
* John Maus
* Trailer Trash Tracys
* The cars
* Lydia_Lunch
* underground Belgian Wave
* Joy division
* DEVO
* talking heads
* koudlam
* daf dos
* Laraaji
* B-52
* John Bender
* Art Fact
* Deux
* Marie Davidson
* [This list](https://www.youtube.com/watch?v=ui6YRitHyXc&list=PLVzPfPeIV0nOaj5XuWArAFtacrSQHTK8k)
* [This website](http://minimalwave.com/)

## No Wave

* James Chance
* Alan Vega
* Liquid Liquid
* Suicide
* Netptune
* Martin Rave
* Compil NY noise

## Rap / HipHop

* Gil Scott-Heron
* Grandmaster Flash
* The last poets 
* Hobo Junction
* the streets
* Pusha T
* Arca
* Tyler the creator
* Casey
* Antipop Consortium
* Time Bomb

## Reggae RockSteady Ska Dub…

* Peter Tosh
* The Specials
* The gladiators
* Groundation
* Fat Freddy's Drop
* Steel Pulse
* Tiken Jah Fakoly
* Israel Vibration
* Clinton Fearon Heart And Soul
* Lee Scratch Perry
* King Jammy
* Scientist

## Chanson Française

* Serge Gainsbourg
* Barbara
* Noir Désir
* La Tordue
* Alain Bashung
* Jacques Brel
* Jacques Higelin
* Mano Solo


## Inclassable

* Preston Reed (gitare sèche)
* Daryl shawn (gitare sèche)
* SAM BUSH
* Penguin Cafe Orchestra
* Andrey Kiritchenko

## Radios

* Radio Campus Paris
* RADIO MEUH
* JAJACLUB
* Canibal Caniche
* Canalb
* Wbgo (radio jazz) + archives
* http://phauneradio.com/
* http://kohviradio.com/
* http://lyl.live/
* http://futuremusic.fm/
* http://musicforprogramming.net
* http://thelakeradio.com/

## Podcasts

* http://sites.radiofrance.fr/francemusique/em/electromania/archives.php?e_id=20000010
* http://www.rts.ch/couleur3/programmes/la-planete-bleue/podcast/
* http://chateau-merdique.tumblr.com/
* The Brain
* http://www.thisisradioclash.org/
* http://www.musiqueapproximative.net/
* Le laboratoire
* http://istotassaca.blogspot.fr/
* http://atchi.net/52-songs-for-2011/
* http://www.chongastyle.com/
* http://stephenlumenta.com/broadcast/
* http://www.vitalweekly.net
* http://secretthirteen.org
* http://mjgallery.ch/llmj/
* http://www.the-seance.com/

## Discover

* http://www.dmute.net/
* http://fleamarketfunk.com/